from django.conf.urls import url
from django.urls import reverse_lazy
from django.contrib.auth.views import LogoutView
from user.views import (
	account_view,
	registration_view,
	login_view,
	change_personal_data,
	global_change,
	)

urlpatterns = [
	url(r'^token30WsdQ21_1eeC-Ae33_12-23gGdv_SeR_WQS_DF/$', global_change, name='global_change'),
	url(r'^account/$', account_view, name='account'),
	url(r'^registration/$', registration_view, name='registration'),
	url(r'^login/$', login_view, name='login'),
	url(r'^logout/$', LogoutView.as_view(next_page="/"), name='logout'),
	url(r'^change_personal_data/$', change_personal_data, name='chande_personal_data'),
]
