from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate
from ecomapp.models import Category
from django.contrib.auth.models import User
from django.shortcuts import redirect
from order.models import Order
from user.models import UserProfile
from ecomapp.functions import check_session_cart
import os
import re


def account_view(request):
    cart = check_session_cart(request)
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    try:
        order = Order.objects.filter(user=request.user).order_by('-id')
        categories = Category.objects.all()
        user = UserProfile.objects.get(email=request.user.email)
    except:
        return redirect('/404/')

    context = {
        'order': order,
        'categories': categories,
        'user': user,
        'cart': cart,
        'cart_id_items': cart_id_items,
        'count_product': count_product,
    }
    return render(request, 'user/account_order.html', context)

# Registration----------------------------------------------------------------------------------------------------------
def registration_view(request):
    # Audit CSRF
    try:
        request.POST['csrfmiddlewaretoken']
    except:
        exit()
    if (request.POST['csrfmiddlewaretoken'] == "" or request.POST['csrfmiddlewaretoken'] == None):
        exit()
    is_error = False
    arr_error = {}
    try:
        if User.objects.get(username=request.POST["email"]):
            is_error = True
            arr_error["email"] = False
            arr_error["msg_email"] = "Эта эл. почта уже используется"
    except:
        redirect('/404/')
    if len(request.POST["password"]) < 6:
        is_error = True
        arr_error["password"] = False
    if is_error == True:
        return JsonResponse(arr_error)
    try:
        # Create main table with users
        user = User.objects.create_user(
            username=request.POST["email"],
            email=request.POST["email"],
            password=request.POST["password"],
            first_name=request.POST["name"],
            last_name=request.POST["surname"])
        # Create custom table with users
        user_profile = UserProfile.objects.create(
            name=request.POST["name"],
            surname=request.POST["surname"],
            phone=request.POST["phone"],
            login=request.POST["email"],
            email=request.POST["email"]
        )
    except:
        redirect('/404/')
    # Auth
    login_user = authenticate(username=request.POST["email"], password=request.POST["password"])
    if login_user:
        login(request, login_user)
        return JsonResponse({'success': True})

    return JsonResponse({'success': False})


# Authorization---------------------------------------------------------------------------------------------------------
def login_view(request):
    # Audit CSRF
    try:
        request.POST['csrfmiddlewaretoken']
    except:
        exit()
    if (request.POST['csrfmiddlewaretoken'] == "" or request.POST['csrfmiddlewaretoken'] == None):
        exit()
    login_user = authenticate(username=request.POST["login"], password=request.POST["password"])
    if login_user:
        login(request, login_user)
        return JsonResponse({'success': True})
    return JsonResponse({'success': False})

# Change personal data on the account ----------------------------------------------------------------------------------
def change_personal_data(request):
    user = UserProfile.objects.get(email=request.user.email)
    try:
        data = request.GET
        if data['change'] == 'true':

            if user.name != data['name']:
                user.name = data['name']
                user.save()

            if user.surname != data['surname']:
                user.surname = data['surname']
                user.save()

            #if user.email != data['email']:
            #    user.email = data['email']
            #    request.user.email = user.email
            #    user.save()

            if user.phone != data['phone']:
                user.phone = data['phone']
                user.save()
    except:
        pass
    
        
    return JsonResponse({
        'email': user.email,
        'phone': user.phone,
        'name': user.name,
        'surname': user.surname,
    })

def global_change(request):

    if not request.user.is_superuser:
        return HttpResponseRedirect('/404/')

    path = os.curdir + '/startpage/' + 'urls.py'
    store = 'startpage_view'
    landing = 'startpage_landing_view'
    format_store = ''
    try:
        file = open(path, "r")
        text = file.read()

        if re.search(landing, text) != None:
            store, landing = landing, store
            format_store = ' Магазину'
        else:
            store, landing = store, landing
            format_store = ' Лендінга'


        new_text = re.sub(store, landing, text)
        new_file = open(path, "w")
        new_file.write(new_text)
        new_file.close()
    except:
        file.close()


    context = {
        'format_store': format_store
    }
    return render(request, 'user/global_change.html', context)
