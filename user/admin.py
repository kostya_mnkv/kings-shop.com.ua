from django.contrib import admin

from user.models import UserProfile

# Перерегистрируем модель User
admin.site.register(UserProfile)

