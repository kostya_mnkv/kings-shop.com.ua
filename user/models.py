from django.db import models

class UserProfile(models.Model):
    name = models.CharField(max_length=120, blank=True)
    surname = models.CharField(max_length=120, blank=True)
    phone = models.CharField(max_length=20, blank=True)
    login = models.CharField(max_length=20, blank=True)
    email = models.CharField(max_length=120, blank=True)

    def __str__(self):
        return str(self.name)+", "+str(self.surname)+" : "+str(self.phone)


# Create your models here.
