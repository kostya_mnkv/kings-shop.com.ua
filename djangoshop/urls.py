
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.views.decorators.cache import cache_page
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('startpage.urls')),
    url(r'^', include('user.urls')),
    url(r'^cart/', include('order.urls')),
    url(r'^', include('ecomapp.urls')),
    url(r'^media/(?P<path>.*)$', cache_page(60 * 60 * 24 * 31)(serve), {
        'document_root': settings.MEDIA_ROOT,
    }),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

