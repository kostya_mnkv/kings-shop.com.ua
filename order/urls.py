from django.conf.urls import url
from django.views.generic import TemplateView
from order.views import (
	cart_view,
	add_to_cart,
	remove_from_cart,
	change_item_qty,
	make_order,
	format_html_for_cart
	)

urlpatterns = [
	# ajax format cart of products
	url(r'^format_html_for_cart/$', format_html_for_cart, name='format_html_for_cart'),
	url(r'^add_to_cart/$', add_to_cart, name='add_to_cart'),
	url(r'^remove_from_cart/$', remove_from_cart, name='remove_from_cart'),
	url(r'^change_item_qty/$', change_item_qty, name='change_item_qty'),
	# View for order page
	url(r'^order/$', cart_view, name='cart'),
	# Register product in DB
	url(r'^make_order/$', make_order, name='make_order'),
	url(r'^thank_you/$', TemplateView.as_view(template_name='order/thanks-page.html'), name='thanks_you'),
]