from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
#from django.core.urlresolvers import reverse
from ecomapp.models import Category, Product
from order.models import CartItem, Cart, Order
from django.contrib.auth.models import User
from user.models import UserProfile
# Session
from ecomapp.functions import check_session_cart

# View for cart page
def cart_view(request):
    cart = check_session_cart(request)
    categories = Category.objects.all()
    # Qty items  in card
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    if count_product == 0:
        return HttpResponseRedirect("/")
    context = {
        'cart': cart,
        'categories': categories,
        'cart_id_items': cart_id_items,
        'count_product': count_product,
    }
    try:
        user = UserProfile.objects.get(email=request.user.email)
        context['user'] = user
    except:
        pass

    return render(request, 'order/order_new.html', context)


# AJAX update for modal-cart
def format_html_for_cart(request):
    cart = check_session_cart(request)
    context = {
        'cart': cart
    }
    return render(request, 'order/format_html_for_cart.html', context)


# AJAX adding  product in cart
def add_to_cart(request):
    cart = check_session_cart(request)
    product_slug = request.GET.get('product_slug')
    product = Product.objects.get(slug=product_slug)
    cart.add_to_cart(product.slug)

    new_cart_total = 0.00
    count_product = 0
    for item in cart.items.all():
        new_cart_total += float(item.item_total)
        count_product = count_product + item.qty

    cart.cart_total = new_cart_total
    cart.save()
    return JsonResponse({
        'cart_total': count_product,
        'cart_total_price': cart.cart_total,

    })


# AJAX remove  product from cart
def remove_from_cart(request):
    cart = check_session_cart(request)
    product_slug = request.GET.get('product_slug')
    product = Product.objects.get(slug=product_slug)
    cart.remove_from_cart(product.slug)
    new_cart_total = 0.00
    count_product = 0
    for item in cart.items.all():
        new_cart_total += float(item.item_total)
        count_product = count_product + item.qty

    cart.cart_total = new_cart_total
    cart.save()
    return JsonResponse({'cart_total': count_product, 'cart_total_price': cart.cart_total, 'product_id': product.id})


# AJAX changing item quantity in cart
def change_item_qty(request):
    cart = check_session_cart(request)
    qty = request.GET.get('qty')
    item_id = request.GET.get('item_id')
    cart.change_qty(qty, item_id)
    cart_item = CartItem.objects.get(id=int(item_id))

    count_product = 0
    for item in cart.items.all():
        count_product = count_product + item.qty

    return JsonResponse(
        {
            'item_id':  cart_item.product_id,
            'item_qty': qty,
            'cart_total': count_product,
            'item_total': cart_item.item_total,
            'cart_total_price': cart.cart_total
        })


# Ajax create order in DB
def make_order(request):
    cart = check_session_cart(request)
    # Audit CSRF
    try:
        request.POST['csrfmiddlewaretoken']
    except:
        exit()
    if (request.POST['csrfmiddlewaretoken'] == "" or request.POST['csrfmiddlewaretoken'] == None):
        exit()
    # Запис товарів для виводу у адмінку
    str_products = ''
    index = 1
    for item in cart.items.all():
        str_products += str(index) + ") " + str(item.product.category) + ' -> ' + str(item.product) + ' (' + str(
            item.qty) + ' кол-во), цена - ' + \
                        str(item.item_total) + ' грн;\n'
        index += 1
    str_products += 'Итого - ' + str(cart.cart_total)

    if request.user.is_authenticated:
        new_order = Order.objects.create(
            user=request.user,
            items=cart,
            products=str_products,
            total=cart.cart_total,
            first_name=request.POST['name'],
            last_name=request.POST['surname'],
            phone=request.POST['phone'],
            address=request.POST['np-name-city'] + ' - ' + request.POST['np-address'],
            buying_type='---',
            comments=request.POST['comment']
        )
    else:
        new_order = Order.objects.create(
            user=User.objects.get(id=1),
            items=cart,
            products=str_products,
            total=cart.cart_total,
            first_name=request.POST['name'],
            last_name=request.POST['surname'],
            phone=request.POST['phone'],
            address=request.POST['np-name-city'] + ' - ' + request.POST['np-address'],
            buying_type='---',
            comments=request.POST['comment']
        )
    del request.session['cart_id']
    del request.session['total']
    return JsonResponse({'status': "okay"})
