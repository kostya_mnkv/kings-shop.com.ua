from decimal import Decimal
from django.conf import settings
from django.db import models

from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from transliterate import translit
from django.contrib.auth.models import User

class Category(models.Model):

	seo_title = models.CharField(max_length=120,blank=True, default="")
	seo_description = models.TextField(blank=True, default="")
	seo_keywords = models.TextField(blank=True, default="")

	name = models.CharField(max_length=100)
	slug = models.SlugField(blank=True)

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse('category_detail', kwargs={'category_slug': self.slug})

def pre_save_category_slug(sender, instance, *args, **kwargs):
	if not instance.slug:
		slug = slugify(translit(instance.name), reversed=True)
		instance.slug = slug

pre_save.connect(pre_save_category_slug, sender=Category)


class Brand(models.Model):

	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class BrandOrderFilter(models.Model):
	category = models.ForeignKey(Category)
	brand = models.ForeignKey(Brand)

	def __str__(self):
		return "{0} - {1}".format(self.brand.name,self.category.name)

def image_folder(instance, filename):
	filename = instance.slug + '.' + filename.split('.')[1]
	return "products/{0}".format(filename)

class ProductManager(models.Manager):

	def all(self):
		return super(ProductManager, self).get_queryset().filter(available=True)

class Product(models.Model):
	category = models.ForeignKey(Category)
	brand = models.ForeignKey(Brand)
	title = models.CharField(max_length=120)
	slug = models.SlugField()
	code_product = models.CharField(max_length=120, default=None, blank=True)
	short_desc = models.TextField(blank=True, default="<br>")
	description = models.TextField()
	price = models.DecimalField(max_digits=9, decimal_places=2)
	stars = models.SmallIntegerField(blank=True, default=5)
	available = models.BooleanField(default=True)
	image = models.ImageField(upload_to=image_folder)
	image2 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image3 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image4 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image5 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image6 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image7 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image8 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	image9 = models.ImageField(upload_to=image_folder, blank=True, default=None)
	seo_title = models.CharField(max_length=120, blank=True, default="")
	seo_description = models.TextField(blank=True, default="")
	seo_keywords = models.TextField(blank=True, default="")
	objects = ProductManager()

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('product_detail', kwargs={'category_slug': self.category.slug, 'product_slug': self.slug})


class Comment(models.Model):
	product = models.ForeignKey(Product)
	author = models.CharField(max_length=100)
	comment = models.TextField()
	data_time = models.DateField(auto_now=True)
