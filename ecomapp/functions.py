from order.models import Cart
from django.http import HttpResponseRedirect

# Create session
def check_session_cart(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
        return cart
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
        return cart


# Redirect to 404
def HttpReturn404():
    return HttpResponseRedirect("/404/")