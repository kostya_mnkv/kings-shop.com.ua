from django.contrib import admin
from ecomapp.models import Category, Brand, Product, BrandOrderFilter, Comment


admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Product)
admin.site.register(BrandOrderFilter)
admin.site.register(Comment)
