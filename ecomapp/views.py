from __future__ import unicode_literals
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from ecomapp.models import Category, Product, BrandOrderFilter, Comment
from order.models import Cart, Order
from django.shortcuts import redirect
from django.core.paginator import Paginator
from user.models import UserProfile
# Session
from ecomapp.functions import check_session_cart

# Serch product in DB
def serch(request):
    try:
        serch_products = Product.objects.all().filter(title__icontains=request.GET["query"])[:5]
    except:
        return redirect('/404/')
    context = {
        "products": serch_products
    }
    return render(request, 'ecomapp/serch.html', context)


# Adding comment for product
def add_comment(request):
    try:
        user = UserProfile.objects.get(email=request.user.email)
    except:
        return redirect('/404/')
    new_comment = Comment.objects.create(
        product_id=request.GET["product_id"],
        author=str(user.name + " " + user.surname),
        comment=request.GET["product_comment"],
    )
    return JsonResponse({'success': "okey"})


# View for detail product page
def product_view(request, category_slug, product_slug):
    cart = check_session_cart(request)
    try:
        product = Product.objects.get(slug=product_slug)
    except:
        return redirect('/404/')
    categories = Category.objects.all()
    comments = Comment.objects.all().filter(product_id=product.id).order_by("-id")
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    context = {
        'comments': comments,
        'product': product,
        'categories': categories,
        'cart': cart,
        'cart_id_items': cart_id_items,
        'count_product': count_product,
    }
    return render(request, 'ecomapp/product.html', context)

# View for detail category page
def category_view(request, category_slug, page_number=1):
    # Корзина
    cart = check_session_cart(request)
    # Отримуємо відповідну категорію по її url
    try:
        category = Category.objects.get(slug=category_slug)
    except:
        return redirect('/404/')
    # Список категорій для меню
    categories = Category.objects.all()
    # Бренди для сортування
    brands = BrandOrderFilter.objects.all().filter(category=category).distinct()
    # Sort products
    type_sort = None
    sort_url = None
    type_from = None
    type_to = None
    products_of_category = Product.objects.filter(category=category)
    if request.GET:
        if request.GET.get('from') or request.GET.get('to'):
            if request.GET.get('from') and request.GET.get('to'):
                type_from = request.GET.get('from')
                type_to = request.GET.get('to')
                products_of_category = Product.objects.filter(category=category).filter(price__gte=type_from).filter(
                    price__lte=type_to).order_by('price')
                sort_url = '?from=' + str(type_from) + '&to=' + str(type_to)
            elif request.GET.get('from'):
                type_from = request.GET.get('from')
                products_of_category = Product.objects.filter(category=category).filter(price__gte=type_from).order_by(
                    'price')
                sort_url = '?from=' + str(type_from)
            elif request.GET.get('to'):
                type_to = request.GET.get('to')
                products_of_category = Product.objects.filter(category=category).filter(price__lte=type_to).order_by(
                    'price')
                sort_url = '?to=' + str(type_to)
        elif request.GET.get('sort'):
            type_sort = request.GET.get('sort')
            if type_sort == 'desc':
                products_of_category = Product.objects.filter(category=category).order_by('-price')
                sort_url = '?sort=' + type_sort
            elif type_sort == 'asc':
                products_of_category = Product.objects.filter(category=category).order_by('price')
                sort_url = '?sort=' + type_sort
            else:
                products_of_category = Product.objects.filter(category=category).filter(brand__name=type_sort)
                sort_url = '?sort=' + type_sort
        else:
            products_of_category = Product.objects.filter(category=category)
    else:
        products_of_category = Product.objects.filter(category=category)

    # Paginator
    try:
        current_page = Paginator(products_of_category, 15)
    except:
        return redirect('/404/')
    # Count products, id products for print active product in the card
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    # Дані які передаємо в шаблон
    context = {
        'category': category,
        'products_of_category': current_page.page(page_number),
        'category_slug': category_slug,
        'categories': categories,
        'cart': cart,
        'cart_id_items': cart_id_items,
        'count_product': count_product,
        'sort_url': sort_url,
        'type_from': type_from,
        'type_to': type_to,
        'brands': brands,
    }
    return render(request, 'ecomapp/category.html', context)
