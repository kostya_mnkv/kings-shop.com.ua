from django.conf.urls import url
from django.views.generic import TemplateView
from ecomapp.views import (
	category_view, 
	product_view,
	add_comment,
	serch
	)

urlpatterns = [
	url(r'^query_serch/$', serch, name='query_serch'),
	url(r'^thank_you/$', TemplateView.as_view(template_name='order/thank_you.html'), name='thank_you'),
	url(r'^add_comment/$', add_comment, name='add_comment'),
	# Детальна сторінка категорії
	url(r'^(?P<category_slug>[-\w]+)/(?P<page_number>[\d+])/$', category_view, name='category_detail'),
	url(r'^(?P<category_slug>[-\w]+)/$', category_view, name='category_detail'),
	# Детальна сторінка  продукту
	url(r'^(?P<category_slug>[-\w]+)/(?P<product_slug>[-\w]+)/$',product_view, name='product_detail'),
]
