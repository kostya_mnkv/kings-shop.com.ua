$(document).ready(function () {
 $(".third_form").addClass("active");
        $(".ordering_levels .third_level").addClass("active");
        $(".ordering_levels .second_level").removeClass("active");
        $(".second_form").addClass("completed").removeClass('active')
});
$(function () {
    var SHOW_CLASS = 'show', HIDE_CLASS = 'hide', ACTIVE_CLASS = 'active';
    $('.tabs').on('click', 'li a', function (e) {
        e.preventDefault();
        var $tab = $(this), href = $tab.attr('href');
        $('.active').removeClass(ACTIVE_CLASS);
        $tab.addClass(ACTIVE_CLASS);
        $('.show').removeClass(SHOW_CLASS).addClass(HIDE_CLASS).hide();
        $(href).removeClass(HIDE_CLASS).addClass(SHOW_CLASS).hide().fadeIn(550)
    })
});
$(document).ready(function () {
    let enter = $('#modal-enter');
    let exit = $('#modal-exit');
    $('#modal-action-exit').on('click', function () {
        enter.css({'transform': 'translateX(-115%)'});
        exit.css({'transform': 'translateX(0%)'});
        $('.modal-body').css('height', '27rem');
        $('#modal-action-exit').addClass('exit_active')
        $('#modal-action-enter').removeClass('exit_active')
    });
    $('#modal-action-enter').on('click', function () {
        enter.css({'transform': 'translateX(0%)'});
        exit.css({'transform': 'translateX(115%)'});
        $('.modal-body').css('height', 'auto');
        $('#modal-action-enter').addClass('exit_active')
        $('#modal-action-exit').removeClass('exit_active')
    })
});
$(function () {
    $("#phone").mask("+38(999) 999-9999")
})