from django.conf.urls import url
from startpage.views import startpage_view, handler404, feedback, sertificat, ajax_third_section, \
    ajax_second_section, ajax_first_section
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', startpage_view, name='startpage'),
    url(r'feedback/$', feedback, name='feedback'),
    url(r'ajax_third_section/$', ajax_third_section, name='ajax_third_section'),
    url(r'ajax_second_section/$', ajax_second_section, name='ajax_second_section'),
    url(r'ajax_first_section/$', ajax_first_section, name='ajax_first_section'),
    url(r'^404/$', handler404, name='404'),
]
