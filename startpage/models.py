from django.db import models
from order.models import Product
import datetime


def image_folder(instance, filename):
    filename = instance.product.slug + '.' + filename.split('.')[1]
    return "products/{0}".format(filename)


class Slider(models.Model):
    title = models.CharField(max_length=255)
    product = models.ForeignKey(Product)
    image = models.ImageField(upload_to=image_folder)

    def __str__(self):
        return self.title


class SliderLanding(models.Model):
    title = models.CharField(max_length=255)
    product = models.ForeignKey(Product)
    image = models.ImageField(upload_to=image_folder)

    def __str__(self):
        return self.title


class FirstBlockContent(models.Model):
    product = models.ForeignKey(Product)

    def __str__(self):
        return self.product.title


class FirstBlockTitle(models.Model):
    title = models.CharField(max_length=120)

    def __str__(self):
        return self.title


class SecondBlockContent(models.Model):
    product = models.ForeignKey(Product)

    def __str__(self):
        return self.product.title


class SecondBlockTitle(models.Model):
    title = models.CharField(max_length=120)

    def __str__(self):
        return self.title


class ThirdBlockContent(models.Model):
    product = models.ForeignKey(Product)

    def __str__(self):
        return self.product.title


class ThirddBlockTitle(models.Model):
    title = models.CharField(max_length=120)

    def __str__(self):
        return self.title

class SEO(models.Model):

    title = models.TextField(blank=True, default="")
    description = models.TextField(blank=True, default="")
    keywords = models.TextField(blank=True, default="")

class Feedback(models.Model):
    phone = models.CharField(max_length=15, blank=False)
    date = models.CharField(max_length=20,blank=True, default=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))

    def __str__(self):
        return "Телефон " + self.phone + " , дата  " + self.date
