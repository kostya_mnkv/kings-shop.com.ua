from django.shortcuts import render
from ecomapp.models import Category
from django.shortcuts import render_to_response
from django.template import RequestContext
from order.models import Cart
from django.http import HttpResponseRedirect, JsonResponse
from startpage.models import Feedback
from django.core.cache import cache
from django.views.decorators.cache import cache_page

# Session
from ecomapp.functions import check_session_cart
from startpage.models import (
    Slider,
    FirstBlockContent,
    FirstBlockTitle,
    SecondBlockContent,
    SecondBlockTitle,
    ThirdBlockContent,
    ThirddBlockTitle,
    SEO,
    SliderLanding,
)


def startpage_view(request):
    # Перевірка корзини
    cart = check_session_cart(request)

    sliders = Slider.objects.all()
    categories = Category.objects.all()

    seo = SEO.objects.get(id=1)
    # Count products, id products for print active product in the card
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    # Data for template
    context = {
        'cart_id_items': cart_id_items,

        'categories': categories,
        'cart': cart,
        'sliders': sliders,
        'count_product': count_product,
        'seo': seo,
    }
    return render(request, 'startpage/index.html', context)


def sertificat(request):
    return render(request, 'startpage/347A212FC6F584C220A75FCD7FADE44A.txt', {})


def startpage_landing_view(request):
    # Перевірка корзини
    cart = check_session_cart(request)

    if cache.get('landing_slider'):
        sliders = cache.get('landing_slider')
        print("EWEWEEW")
    else:
        sliders = SliderLanding.objects.all()
        cache.set('landing_slider', sliders, 9999)

    if cache.get('landing_category'):
        categories = cache.get('landing_category')
        print("_category")
    else:
        categories = Category.objects.all()
        cache.set('landing_category', categories, 9999)

    if cache.get('landing_seo'):
        seo = cache.get('landing_seo')
        print("_seo")
    else:
        seo = Category.objects.all()
        cache.set('landing_seo', categories, 9999)


    # Count products, id products for print active product in the card
    count_product = 0
    cart_id_items = []
    cart_id_items.clear()
    for item in cart.items.all():
        count_product = count_product + item.qty
        cart_id_items.append(item.product.id)
    # Data for template
    context = {

        'cart_id_items': cart_id_items,
        'categories': categories,
        'cart': cart,
        'sliders': sliders,
        'count_product': count_product,
        'seo': seo,
    }
    return render(request, 'startpage/landing_index.html', context)



def feedback(request):
    try:
        request.GET['phone']
    except:
        return JsonResponse({
        'success': False
    })

    new_feedback = Feedback.objects.create(
        phone=request.GET['phone'],
    )

    return JsonResponse({
        'success': True
    })


@cache_page(60 * 15, key_prefix="third_section")
def ajax_third_section(request):
    third_title = ThirddBlockTitle.objects.get(id=1)
    third_content = ThirdBlockContent.objects.all()
    context = {
        'third_title': third_title,
        'third_content': third_content,
    }
    return render(request, 'startpage/get_ajax_3section_html.html', context)


@cache_page(60 * 15, key_prefix="second")
def ajax_second_section(request):
    second_title = SecondBlockTitle.objects.get(id=1)
    second_content = SecondBlockContent.objects.all()

    context = {
        'second_title': second_title,
        'second_content': second_content,
    }
    return render(request, 'startpage/get_ajax_2section_html.html', context)


@cache_page(60 * 15, key_prefix="first_section")
def ajax_first_section(request):
    first_title = FirstBlockTitle.objects.get(id=1)
    first_content = FirstBlockContent.objects.all()
    context = {

        'first_title': first_title,
        'first_content': first_content,
    }
    return render(request, 'startpage/get_ajax_1section_html.html', context)

def handler404(request):
    response = render_to_response('ecomapp/404.html')
    response.status_code = 404
    return response