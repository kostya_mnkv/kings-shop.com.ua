from django.contrib import admin
from startpage.models import (Slider,
                              FirstBlockContent,
                              FirstBlockTitle,
                              SecondBlockContent,
                              SecondBlockTitle,
                              ThirdBlockContent,
                              ThirddBlockTitle,
                              SEO,
                              Feedback,
                              SliderLanding)

admin.site.register(Slider)
admin.site.register(SliderLanding)
admin.site.register(FirstBlockTitle)
admin.site.register(FirstBlockContent)
admin.site.register(SecondBlockTitle)
admin.site.register(SecondBlockContent)
admin.site.register(ThirddBlockTitle)
admin.site.register(ThirdBlockContent)
admin.site.register(SEO)
admin.site.register(Feedback)
